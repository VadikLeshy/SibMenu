#include "classic_menu_viewer.h"
#include "modern_menu_viewer.h"
#include "modern_handler.h"
#include <QApplication>
#include <QSettings>
#include <iostream>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  QSettings s("Soft by Vadik", "Main Menu");
  menu_element::prefer_theme_icons = s.value("Prefer_theme_icons", false).toBool();
  menu_element::disable_icons = s.value("Disable_icons", false).toBool();
  menu::set_update_interval(s.value("Pipe_update_interval", -1).toInt());
  QFont fnt;
  fnt = QFont(s.value("Font_name", fnt.family()).toString(), s.value("Font_size", -1).toInt(), s.value("Font_weight", -1).toInt(),
              s.value("Font_is_italic", false).toBool());
  menu m("", "", "");
  m.update();

  QString style = s.value("Style", "Classic").toString();
  int i = a.arguments().indexOf("--menu_style");
  if(i != -1 && i+1 < a.arguments().size())
    style = a.arguments()[i+1];
  QWidget *v;
  if(style == "Modern")
    {
      v = new modern_handler(new modern_menu_viewer(&m));
      modern_menu_viewer::close_on_action = s.value("Close_on_action", true).toBool();
    } else
    if(style == "Classic")
      {
        v = new classic_menu_viewer(&m, fnt);
        QObject::connect(v, SIGNAL(aboutToHide()), &a, SLOT(quit()));
      } else
      {
        std::cerr << "Unsupported menu style" << std::endl;
        return 0;
      }

  v->setFont(fnt);
  v->show();
  int result = a.exec();
  v->deleteLater();
  std::cerr << "Корректное завершение" << std::endl;
  return result;
}
