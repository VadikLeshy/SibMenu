#ifndef ACTION_INVALID_H
#define ACTION_INVALID_H

#include "action.h"

class action_invalid: public action
{
  QString type;
public:
  action_invalid(const QString &type);

  void execute(menu_element *);

  inline virtual QString description() const {return "Unsupported action: "+type;}
};

#endif // ACTION_INVALID_H
