#include "modern_menu_viewer.h"
#include "iconed_label.h"
#include <QCommandLinkButton>
#include <QFrame>
#include <QVariant>
#include <QResizeEvent>
#include <QScrollBar>

bool modern_menu_viewer::close_on_action = true;

void modern_menu_viewer::expose_submenu(modern_menu_viewer *v)
{
  v->setFont(font());
  v->setGeometry(geometry());
  hide();
  v->show();
}

void modern_menu_viewer::showEvent(QShowEvent *e)
{
  emit aboutToShow();
  QScrollArea::showEvent(e);
  if(dirty)
    update_content();
}

void modern_menu_viewer::resizeEvent(QResizeEvent *e)
{
  QScrollArea::resizeEvent(e);
  if(widget())
    widget()->resize(e->size().width(), widget()->height());
}

void modern_menu_viewer::scrollContentsBy(int dx, int dy)
{
  sc_x += dx;
  sc_y += dy;
  QScrollArea::scrollContentsBy(dx, dy);
}

modern_menu_viewer::modern_menu_viewer(menu *m0, QWidget *parent):
  QScrollArea(parent), w(0), l(new QVBoxLayout), m(0), dirty(false),
  sc_x(0), sc_y(0), old_sc_x(0), old_sc_y(0)
{
  if(m0)
    set_menu(m0);
}

modern_menu_viewer::~modern_menu_viewer()
{
  for(modern_menu_viewer *v: child_menus)
    v->deleteLater();
  clear();
  delete l;
}

void modern_menu_viewer::set_menu(menu *m0)
{
  while(m0 && m0->get_children().size() == 1)
    {
      menu *m1 = dynamic_cast<menu*>(m0->get_children()[0]);
      if(m1)
        m0 = m1;
      else
        break;
    }
  if(m)
    {
      disconnect(m, SIGNAL(update_finished()), this, SLOT(menu_updated()));
      disconnect(this, SIGNAL(aboutToShow()), m, SLOT(update_from_pipe()));
    }
  m = m0;
  if(m)
    {
      connect(m, SIGNAL(update_finished()), SLOT(menu_updated()));
      connect(this, SIGNAL(aboutToShow()), m, SLOT(update_from_pipe()));
      menu_updated();
    }
}

void modern_menu_viewer::add_menu(modern_menu_viewer *m0, const QString &title, const QIcon &ico)
{
  m0->setProperty("ParentViewer", (quint64)this);
  QCommandLinkButton *btn = new QCommandLinkButton(title);
  btn->setDescription(m0->is_pipe()? "Submenu: pipe menu": QString("Submenu: %1 items").arg(m0->valid_items_count()-1));
  btn->setIcon(ico);
  btn->setProperty("Menu", (quint64)m0);
  btn->setFlat(true);
  connect(btn, SIGNAL(clicked(bool)), SLOT(expose_submenu()));
  l->addWidget(btn);
}

void modern_menu_viewer::addSection(const QIcon &ico, const QString &str)
{
  iconed_label *lbl = new iconed_label(str, ico);
  l->addWidget(lbl);
  QFrame *f = new QFrame;
  f->setFrameShape(QFrame::HLine);
  l->addWidget(f);
}

void modern_menu_viewer::addAction(const QIcon &icon, const QString &text, const QString &desc, const QObject *receiver, const char *member, const QKeySequence &shortcut)
{
  QCommandLinkButton *btn = new QCommandLinkButton(text, desc);
  btn->setIcon(icon);
  btn->setShortcut(shortcut);
  btn->setFlat(true);
  connect(btn, SIGNAL(clicked(bool)), receiver, member, Qt::DirectConnection);
  connect(btn, SIGNAL(clicked(bool)), SIGNAL(ask_close()), Qt::DirectConnection);
  l->addWidget(btn);
}

int get_buttons(QLayout *l)
{
  int c = 0;
  for(int i = 0; i<l->count(); ++i)
    {
      QLayoutItem *it = l->itemAt(i);
      if(it)
        {
          if(dynamic_cast<QAbstractButton*>(it->widget()))
            ++c;
          else
            if(it->layout())
              c += get_buttons(it->layout());
        }
    }
  return c;
}

int modern_menu_viewer::valid_items_count()
{
  return get_buttons(l);
}

void modern_menu_viewer::setWidget(QWidget *widget)
{
  QScrollArea::setWidget(widget);
  if(widget)
    widget->resize(width(), widget->height());
  setMinimumWidth(widget->sizeHint().width() + 2*l->margin());
}

void modern_menu_viewer::clear()
{
  old_sc_x = sc_x;
  old_sc_y = sc_y;
  while(l->count())
    {
      QLayoutItem *it = l->itemAt(0);
      QWidget *el = it->widget();
      if(el)
        el->deleteLater();
      l->removeItem(it);
    }
}

void modern_menu_viewer::expose_submenu()
{
  bool ok;
  modern_menu_viewer *v = (modern_menu_viewer*)sender()->property("Menu").toULongLong(&ok);
  if(ok)
    emit ask_viewer_change(v);
}

void modern_menu_viewer::hide_submenu()
{
  bool ok;
  modern_menu_viewer *v = (modern_menu_viewer*)property("ParentViewer").toULongLong(&ok);
  if(ok)
    emit ask_viewer_change(v);
}

void modern_menu_viewer::update_content()
{
  if(w && takeWidget() == w)
    w->deleteLater();
  w = new QWidget;
  w->setLayout(l);
  setWidget(w);
  dirty = false;
  verticalScrollBar()->setValue(-old_sc_y);
  horizontalScrollBar()->setValue(-old_sc_x);
}

void modern_menu_viewer::menu_updated()
{
  clear();
  for(menu_element *el: m->get_children())
    {
      menu *m1 = dynamic_cast<menu*>(el);
      if(m1)
        {
          modern_menu_viewer *qm = new modern_menu_viewer;
          qm->set_menu(m1);
          qm->addAction(QIcon::fromTheme("back", QIcon::fromTheme("go-back")), "Back", "Go to parrent menu", qm, SLOT(hide_submenu()));
          add_menu(qm, m1->get_caption(), m1->get_icon());
          child_menus.append(qm);
        } else {
          if(el->is_separator())
            addSection(el->get_icon(), el->get_caption());
          else
            addAction(el->get_icon(), el->get_caption(), el->get_description(), el, SLOT(execute()));
        }
    }
  if(isVisible())
    update_content();
  else
    dirty = true;

  bool ok;
  property("ParentViewer").toULongLong(&ok);
  if(ok)
    addAction(QIcon::fromTheme("back", QIcon::fromTheme("go-back")), "Back", "Go to parrent menu", this, SLOT(hide_submenu()));
}
