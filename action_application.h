#ifndef ACTION_APPLICATION_H
#define ACTION_APPLICATION_H

#include "action.h"

class action_application: public action
{
  QString program;
  friend class menu;
public:
  action_application(const QString &app_path);
  void execute(menu_element *);

  virtual QString description() const;
};

#endif // ACTION_APPLICATION_H
