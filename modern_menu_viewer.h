#ifndef MODERN_MENU_VIEWER_H
#define MODERN_MENU_VIEWER_H

#include <QScrollArea>
#include <QVBoxLayout>
#include <QPointer>
#include "menu.h"

class modern_menu_viewer : public QScrollArea
{
  Q_OBJECT

  QWidget *w;
  QVBoxLayout *l;
  QVector<modern_menu_viewer*> child_menus;
  menu *m;

  bool dirty;

  int sc_x;
  int sc_y;

  int old_sc_x;
  int old_sc_y;

protected:
  void expose_submenu(modern_menu_viewer *v);

  void showEvent(QShowEvent *);

  void resizeEvent(QResizeEvent *);

  void scrollContentsBy(int dx, int dy);

public:
  modern_menu_viewer(menu *m0 = 0, QWidget *parent = 0);
  ~modern_menu_viewer();

  void set_menu(menu *m0);

  void add_menu(modern_menu_viewer *m0, const QString &title, const QIcon &ico);

  void addSection(const QIcon &ico, const QString &str);
  void addAction(const QIcon &icon, const QString &text, const QString &desc, const QObject *receiver, const char* member, const QKeySequence &shortcut = 0);

  inline int count() {return l->count();}
  int valid_items_count();
  inline bool is_pipe() {return m && m->is_pipe();}

  void setWidget(QWidget *widget);

  static bool close_on_action;

public slots:
  void clear();

protected slots:
  void expose_submenu();
  void hide_submenu();

  void update_content();
  void menu_updated();

signals:
  void aboutToShow();

  void ask_viewer_change(modern_menu_viewer *v);

  void ask_close();
};

#endif // MODERN_MENU_VIEWER_H
