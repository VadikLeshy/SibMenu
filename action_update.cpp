#include "action_update.h"
#include "menu.h"

action_update::action_update()
{

}

void action_update::execute(menu_element *e)
{
  menu *m = e->get_parent();
  while(m->get_parent())
    m = m->get_parent();
  m->update();
}
