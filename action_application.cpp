#include "action_application.h"
#include <QProcess>
#include <QMessageBox>

action_application::action_application(const QString &app_path):
  program(app_path)
{
}

void action_application::execute(menu_element *)
{
  if(!QProcess::startDetached("sh -c \""+program+"\""))
    if(!QProcess::startDetached("bash -c \""+program+"\""))
      if(!QProcess::startDetached(program))
        QMessageBox::critical(0, "Error", "Can not run \""+program+"\"!");
}

QString action_application::description() const
{
  QProcess p;
  QString str = QString("sh -c \"echo -n %1\"").arg(program);
  p.start(str, QProcess::ReadOnly | QProcess::Text);
  p.waitForStarted();
  p.waitForFinished();
  str = p.readAll();
  return "Execute \""+str+"\"";
}
