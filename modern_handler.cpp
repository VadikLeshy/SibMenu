#include "modern_handler.h"
#include <QProcess>
#include <QMessageBox>
#include <QAction>

modern_handler::modern_handler(modern_menu_viewer *v, QWidget *parent):
  QWidget(parent), l(new QVBoxLayout), e(new QLineEdit)
{
  l->setMargin(2);
  l->addWidget(v);
  connect(e, SIGNAL(returnPressed()), SLOT(start_program()));
  l->addWidget(e);
  connect(v, SIGNAL(ask_viewer_change(modern_menu_viewer*)), SLOT(change_viewer(modern_menu_viewer*)));
  connect(v, SIGNAL(ask_close()), SLOT(close()));
  setLayout(l);
  setWindowTitle("Sib Menu");
  setWindowIcon(QIcon::fromTheme("start-here", QIcon::fromTheme("distributor-logo", QIcon::fromTheme("gnome-main-menu"))));
  resize(320, 480);
  QAction *exit_action = new QAction(this);
  exit_action->setShortcut(QKeySequence("Ctrl+Q"));
  connect(exit_action, SIGNAL(triggered(bool)), SLOT(close()));
  addAction(exit_action);
}

modern_handler::~modern_handler()
{
}

void modern_handler::change_viewer(modern_menu_viewer *target)
{
  modern_menu_viewer *old = dynamic_cast<modern_menu_viewer*>(sender());
  disconnect(old, 0, this, 0);
  connect(target, SIGNAL(ask_viewer_change(modern_menu_viewer*)), SLOT(change_viewer(modern_menu_viewer*)));
  connect(target, SIGNAL(ask_close()), SLOT(close()));
  old->hide();
  l->replaceWidget(old, target);
  target->setHidden(false);
}

void modern_handler::start_program()
{
  bool b;
  if(! (b=QProcess::startDetached(e->text())))
    {
      QStringList lst;
      lst << "-c" << e->text();
      if(! (b=QProcess::startDetached("sh", lst)))
        if(! (b=QProcess::startDetached("bash", lst)))
          QMessageBox::critical(0, "Error", "Can not run \""+e->text()+"\"!");
    }
  if(b)
    e->setText("");
}
