#include "iconed_label.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QIcon>

iconed_label::iconed_label(const QString &text, const QIcon &ico, QWidget *parent) : QWidget(parent)
{
  QLabel *lbl0 = new QLabel(text), *lbl1 = new QLabel;
  if(!ico.isNull())
    lbl1->setPixmap(ico.pixmap(32, 32));
  lbl1->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  lbl0->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

  QHBoxLayout *l=new QHBoxLayout;
  l->addWidget(lbl1);
  l->addWidget(lbl0);
  setLayout(l);
}
