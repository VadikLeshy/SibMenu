#-------------------------------------------------
#
# Project created by QtCreator 2016-01-23T11:51:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SibMenu
TEMPLATE = app

CONFIG += c++14

SOURCES += main.cpp\
    menu_element.cpp \
    menu.cpp \
    action_application.cpp \
    action_update.cpp \
    action_invalid.cpp \
    classic_menu_viewer.cpp \
    modern_menu_viewer.cpp \
    iconed_label.cpp \
    modern_handler.cpp

HEADERS  += \
    menu_element.h \
    menu.h \
    action.h \
    action_application.h \
    action_update.h \
    action_invalid.h \
    classic_menu_viewer.h \
    modern_menu_viewer.h \
    iconed_label.h \
    modern_handler.h

FORMS    +=

unix {
    target.path = /usr/bin
    INSTALLS += target
}
