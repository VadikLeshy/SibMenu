#ifndef CLASSIC_MENU_VIEWER_H
#define CLASSIC_MENU_VIEWER_H

#include <QMenu>
#include "menu.h"

class classic_menu_viewer : public QMenu
{
  Q_OBJECT
  menu *m;

protected:
  void showEvent(QShowEvent *);

public:
  classic_menu_viewer(menu *m0 = 0, QFont fnt = QFont());
  ~classic_menu_viewer();

  void set_menu(menu *m);

protected slots:
  void menu_updated();
};

#endif // CLASSIC_MENU_VIEWER_H
