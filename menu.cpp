#include "menu.h"
#include "action_application.h"
#include "action_update.h"
#include "action_invalid.h"

#include <QFile>
#include <QProcess>
#include <QDir>
#include <QXmlStreamReader>

#include <iostream>

QTimer* default_timer()
{
  QTimer *t = new QTimer;
  t->setInterval(-1);
  return t;
}

QTimer* menu::update_timer = default_timer();

menu::menu(const QString &caption, const QString &icon, const QString &id, menu *parent):
  menu_element(caption, icon, parent), id(id)
{
  connect(update_timer, SIGNAL(timeout()), SLOT(update_from_pipe()));
  if(!update_timer->isActive() && update_timer->interval() > -1)
    update_timer->start();
}

menu::~menu()
{
  for(menu_element *el: children)
    el->deleteLater();
}

void menu::set_update_interval(int msec)
{
  if(msec == -1)
    update_timer->stop();
  update_timer->setInterval(msec);
}

bool hasError(const QProcess &p)
{
  return (p.exitCode() || p.exitStatus() == QProcess::CrashExit) &&
      p.state() != QProcess::Running;
}

void menu::update_from_pipe()
{
  if(pipe_command.isEmpty()) return;

  QProcess p;
  p.start("sh", {"-c", pipe_command}, QProcess::ReadOnly);
  p.waitForStarted();

  if(!hasError(p))
    update(&p);
}

void menu::update()
{
  QFile f(QDir::homePath()+"/.config/openbox/menu.xml");
  f.open(QFile::ReadOnly);
  update(&f);
  f.close();
}

QString extract_icon(QString prog_path)
{
  prog_path = prog_path.section(" ", 0, 0);
  prog_path = prog_path.section("/", prog_path.count("/"));
  return prog_path;
}

void menu::update(QIODevice * const src)
{
  clear();
  if(!src->isOpen())
    return;
  QXmlStreamReader parser(src);
  menu *current_menu = this;
  action *act = 0;
  while(!parser.atEnd())
    {
      do
        {
          parser.device()->waitForReadyRead(INT_MAX);
          parser.readNext();
        } while(parser.error() == QXmlStreamReader::PrematureEndOfDocumentError);
      if(parser.hasError())
        {
          std::cerr << "XML error! " << parser.errorString().toStdString() << std::endl;
          continue;
        }
      if(parser.isComment())
        {
          std::cout << parser.text().toString().toStdString() << std::endl;
          continue;
        }
      QString key = parser.name().toString().toLower();
      if(key == "menu")
        {
          if(parser.tokenType() == QXmlStreamReader::EndElement)
            current_menu = current_menu->get_parent();
          else
            {
              QXmlStreamAttributes a = parser.attributes();
              QString id = a.value("id").toString();
              auto el = menus.find(id);
              if(el == menus.end())
                {
                  el = menus.insert(id, new menu(a.value("label").toString(), a.value("icon").toString(), id, current_menu));
                  (*el)->pipe_command = a.value("execute").toString();
                }
              current_menu->children.append(*el);
              current_menu = *el;
            }
        } else
        if(key == "item" || key == "separator")
          {
            if(parser.tokenType() == QXmlStreamReader::EndElement)
              continue;
            QXmlStreamAttributes a = parser.attributes();
            current_menu->children.append(new menu_element(a.value("label").toString(), a.value("icon").toString(), current_menu));
            if(key == "separator")
              current_menu->children.back()->sep = true;
          } else
          if(key == "action")
            {
              if(parser.tokenType() == QXmlStreamReader::EndElement)
                continue;
              QXmlStreamAttributes a = parser.attributes();
              QString name = a.value("name").toString().toLower();
              if(name == "execute")
                act = new action_application("");
              else
                if(name == "reconfigure")
                  act = new action_update;
                else
                  act = new action_invalid(name);
              current_menu->children.last()->actions.append(act);
            } else
            if(key == "execute" || key == "command")
              {
                if(parser.tokenType() == QXmlStreamReader::EndElement)
                  continue;
                action_application *act_app = dynamic_cast<action_application*>(act);
                if(act_app)
                  {
                    act_app->program = parser.readElementText();
                    if(current_menu->children.last()->icon.isEmpty())
                      current_menu->children.last()->icon = extract_icon(act_app->program);
                  }
              }
    }
  emit update_finished();
}

void menu::clear()
{
  for(menu_element *el: children)
    {
      if(el)
        el->deleteLater();
    }
  children.clear();
}
