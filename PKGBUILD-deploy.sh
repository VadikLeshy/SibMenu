#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"
makepkg -f
pkexec pacman --noconfirm -U "$DIR/"*.pkg*

